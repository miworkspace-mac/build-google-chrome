#!/bin/bash -ex

# CONFIG
icon_name=""
category="Productivity"
description="This update contains stability and security fixes for the Chrome Web Browser"

# download it
curl -o app.pkg https://dl.google.com/chrome/mac/stable/accept_tos%3Dhttps%253A%252F%252Fwww.google.com%252Fintl%252Fen_ph%252Fchrome%252Fterms%252F%26_and_accept_tos%3Dhttps%253A%252F%252Fpolicies.google.com%252Fterms/googlechrome.pkg

## Unpack
/usr/sbin/pkgutil --expand-full app.pkg pkg
mkdir -p build-root/Applications
cp -R pkg/*.pkg/Payload/*.app build-root/Applications

#Obtain version info
version=`/usr/libexec/PlistBuddy -c "print :CFBundleShortVersionString"  build-root/Applications/*.app/Contents/Info.plist`

## Build pkginfo (this is done through an echo to expand key_files)
/usr/local/munki/makepkginfo app.pkg -f build-root/Applications/Google\ Chrome.app > app.plist

## Fixup and remove "build-root" from file paths
perl -p -i -e 's/build-root//' app.plist

plist=`pwd`/app.plist

# Change path and other details in the plist
defaults write "${plist}" installer_item_location "jenkins/Google-Chrome-${version}.pkg"
defaults write "${plist}" name "Chrome"
defaults write "${plist}" minimum_os_version "10.13.0"
defaults write "${plist}" uninstallable -bool NO
defaults write "${plist}" unattended_install -bool YES
defaults write "${plist}" icon_name "${icon_name}"
defaults write "${plist}" category "${category}"
defaults write "${plist}" description -string "${description}"
defaults write "${plist}" unattended_install -bool TRUE


# Add Blocking Applications array
BlockApp1="Google Chrome.app"
defaults write "${plist}" blocking_applications -array "${BlockApp1}"

# Make readable by humans
/usr/bin/plutil -convert xml1 "${plist}"
chmod a+r "${plist}"

# Change filenames to suit
mv app.pkg Google-Chrome-${version}.pkg
mv "${plist}" Google-Chrome-${version}.plist

#https://dl.google.com/chrome/mac/stable/accept_tos%3Dhttps%253A%252F%252Fwww.google.com%252Fintl%252Fen_ph%252Fchrome%252Fterms%252F%26_and_accept_tos%3Dhttps%253A%252F%252Fpolicies.google.com%252Fterms/googlechrome.pkg