#!/bin/bash -ex

url="https://dl.google.com/chrome/mac/universal/stable/GGRO/googlechrome.dmg"

curl -L -o chrome.dmg -A 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/46.0.2490.80 Safari/537.36' "${url}"

mountpoint=`yes | hdiutil attach -mountrandom /tmp -nobrowse chrome.dmg | awk '/private\/tmp/ { print $3 } '`

mkdir -p build-root/Applications
app_in_dmg=$(ls -d $mountpoint/*.app)
cp -R "$app_in_dmg" build-root/Applications

# Obtain version info
VERSION=`/usr/libexec/PlistBuddy -c "Print :CFBundleShortVersionString" "$app_in_dmg"/Contents/Info.plist | awk '{ print $1}'`

if [ "x${VERSION}" != "x" ]; then
    echo VERSION: "${VERSION}"
    echo "${VERSION}" > current-version
fi

hdiutil detach -quiet "${mountpoint}"

rm -rf chrome.dmg

# Update to handle distributed builds
if cmp current-version old-version; then
    # Files are identical, exit 1 to NOT trigger the build job
    exit 1
else
    # Files are different - copy marker, exit 0 to trigger build job
    cp current-version old-version
    exit 0
fi

